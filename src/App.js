import React, {useEffect, useState} from 'react';
import './App.css';

function App() {

    const [activePlayer,setActivePlayer]=useState(0);
    const [currentScore0,setCurrentScore0]=useState(0);
    const [currentScore1,setCurrentScore1]=useState(0);
    const [totalScore0,setTotalScore0]=useState(0);
    const [totalScore1,setTotalScore1]=useState(0);
    const [display0,setDisplay0]=useState('inline-block');
    const [display1,setDisplay1]=useState('none');
    const [diceNumber,setDiceNumber]=useState('fas fa-dice-')


    function rollDice() {
        if(activePlayer===0)
        {

            setCurrentScore0((prev)=>{
                const score=Math.floor(Math.random()*6)+1;
                console.log(score,'score000')
                console.log(currentScore0,'currescore0000')
                if(score===1)
                {
                    setDiceNumber('fas fa-dice-'+'one')
                    setActivePlayer(1)
                    return 0
                }
                else if(score===2)
                    setDiceNumber('fas fa-dice-'+'two')
                else if(score===3)
                    setDiceNumber('fas fa-dice-'+'three')
                else if(score===4)
                    setDiceNumber('fas fa-dice-'+'four')
                else if(score===5)
                    setDiceNumber('fas fa-dice-'+'five')
                else if(score===6)
                    setDiceNumber('fas fa-dice-'+'six')

                return prev+score;
            });
        }
        else {
            setCurrentScore1((prev)=>{
                const score=Math.floor(Math.random()*6)+1;
                console.log(score,'score11111')
                console.log(currentScore1,'currescore11111')
                if(score===1)
                {
                    setDiceNumber('fas fa-dice-'+'one')
                    setActivePlayer(0)
                    return 0
                }
                else if(score===2)
                    setDiceNumber('fas fa-dice-'+'two')
                else if(score===3)
                    setDiceNumber('fas fa-dice-'+'three')
                else if(score===4)
                    setDiceNumber('fas fa-dice-'+'four')
                else if(score===5)
                    setDiceNumber('fas fa-dice-'+'five')
                else if(score===6)
                    setDiceNumber('fas fa-dice-'+'six')
                return prev+score;
            });
        }
    }
    useEffect(()=>{
        if(totalScore0>=100){
            alert('Player 1 Wins')
            window.location.reload();
        }
        if(totalScore1>=100)
        {
            alert('Player 2 Wins')
            window.location.reload();
        }
        if(activePlayer===0)
        {
            setDisplay0('inline-block')
            setDisplay1('none')
        }
        if(activePlayer===1)
        {
            setDisplay0('none')
            setDisplay1('inline-block')
        }
    })
    function hold() {

        if(activePlayer===0)
        {
            setTotalScore0((prev)=>{

                return prev+currentScore0
            })
            setActivePlayer(1)
            setCurrentScore0(0)
        }
        else {

            setTotalScore1((prev)=>{
                return prev+currentScore1;
            })
            setActivePlayer(0)
            setCurrentScore1(0)
        }
    }

    function newGame(){
        window.location.reload();
    }
  return (
    <div className='outerContainer'>
        <div className='container'>
            <div className='newGame' onClick={newGame}>
                <i className="fa fa-plus-circle" aria-hidden="true"></i>
                NEW GAME
            </div>
            <div className='dice'>
                <i className={diceNumber}></i>
            </div>
            <div className='player-0 playerDiv'>
                <div className='playerName-0 playerName'>
                    PLAYER 1 <i className="fa fa-circle circle-0" aria-hidden="true" style={{display:display0}}></i>
                </div>
                <div className='playerTotalScore-0 playerTotalScore'>{totalScore0}</div>
                <div className='currentScore-0 currentScore'>
                    <div className='currentText'>Current</div>
                    {currentScore0}
                </div>
            </div>
            <div className='player-1 playerDiv'>
                <div className='playerName-1 playerName'>
                    PLAYER 2 <i className="fa fa-circle circle-1" aria-hidden="true" style={{display:display1}}></i>
                </div>
                <div className='playerTotalScore-1 playerTotalScore'>{totalScore1}</div>
                <div className='currentScore-1 currentScore'>
                    <div className='currentText'>Current</div>
                    {currentScore1}
                </div>
            </div>
            <button className='refresh' onClick={rollDice}><i className="fa fa-refresh" aria-hidden="true"></i> ROLL DICE</button>
            <button className='hold' onClick={hold}><i className="fa fa-arrow-circle-down" aria-hidden="true"></i> HOLD</button>
        </div>
    </div>
  );
}

export default App;
